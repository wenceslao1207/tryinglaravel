<?php

use Illuminate\Http\Request;
use App\Movie;
use App\Actors;
use App\User;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('actors', 'ActorsController');

Route::apiResource('movies', 'MovieController');

Route::post('login', 'AuthController@login');
Route::post('register', 'AuthController@register');

Route::group(['middleware' => 'auth:api'], function() {
	Route::post('getUser', 'AuthController@getUser');
	Route::put('movies', 'MovieController@update');
	Route::delete('movies', 'MovieController@delete');
	Route::get('movies/search/{key}', 'MovieController@find');
	Route::post('movies/create', 'MovieController@create');
	Route::put('actors', 'ActorsController@store');
});

	Route::get('movies', 'MovieController@index');
	Route::get('movies/{id}', 'MovieController@show');
	Route::get('actors', 'ActorsController@index');
