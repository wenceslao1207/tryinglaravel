<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovieActorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('actors_movie', function (Blueprint $table) {
			$table->bigIncrements('id');
		});

		Schema::table('actors_movie', function (Blueprint $table) {
  		  	$table->unsignedBigInteger('actors_id');
			$table->unsignedBigInteger('movie_id');

    		$table->foreign('actors_id')->references('id')->on('actors');
			$table->foreign('movie_id')->references('id')->on('movies');
		});

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('actors_movie');
    }
}
