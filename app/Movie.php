<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    //
	protected $fillable = ['id', 'name', 'description', 'genere'];
	public function actors() {
		return $this->belongsToMany('App\Actors');
	}

}
