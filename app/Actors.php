<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actors extends Model
{
	protected $fillable = ['id', 'name', 'lastname' ];

	public function movies() {
		return $this->belongsToMany('App\Movie');
	}
}
