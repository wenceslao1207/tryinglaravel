<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Actors;
use App\Http\Resources\ActorsResouces;

class ActorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return ActorsResouces::collection(Actors::with('movies')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$actor = Actors::create($request->all());
		$movies = $request->input('movies');
		$actor->movies()->sync($movies);
		return $actor;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$actor = Actors::with('movies')->findOrFail($id);
		return $actor;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$actor = Actors::findOrFail($id);
		$newMovies = $request->input('movies');
		$actor->actors()->sync($newMovies);
		$actor->update($request->all());
		return $actor;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$actor = Actors::findOrFail($id);
		$actor->movies()->detach();
		$actor->delete();

		return 204;
    }
}
