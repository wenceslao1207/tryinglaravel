<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
use App\Http\Resources\MovieResource;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
	{
		return MovieResource::collection(Movie::with('actors')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
		$movie = Movie::create($request->all());
		$actors = $request->input('actors');
		$movie->actors()->sync($actors, false);	
		return $movie;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$movie = Movie::with('actors')->findOrFail($id);
		return $movie;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$movie = Movie::with('actors')->findOrFail($id);
		$newActors = $request->input('actors');
		$movie->actors()->sync($newActors);
		$movie->update($request->all());
		return $movie;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$movie = Movie::findOrFail($id);
		$movie->actors()->detach();
		$movie->delete();

		return 204;
    }

	/**
	 * Find all resources that matches from storage
	 *
	 * @param string $key
	 * @return \iIlluminate\Http\Response
	 */

	public function find($key){
		return Movie::where('name', $key)
					->orderBy('name', "DESC")
					->get();
	}
}

?>
