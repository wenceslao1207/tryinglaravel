export default interface IMovie {
	id: number;
	name: string;
	description: string;
	actors: [];
}
