import Vue from 'vue';
import Router from 'vue-router';
import HomeVue from './views/Home.vue';
import MovieList from './components/MovieList.vue';
import Movie from './components/Movie.vue';
import Login from './components/login.vue';
import createMovie from './components/createMovie.vue';

Vue.use(Router);

export default new Router({
	routes: [
		{
			path: '/',
			name: 'list',
			component: HomeVue,
		},
		{
			path: '/movie/:id',
			name: 'movie',
			component: Movie,
		},
		{
			path: '/login',
			name: 'login',
			component: Login,
		},
		{
			path: '/movie/create',
			name: 'createmovie',
			component: createMovie,
		},
	],
});
