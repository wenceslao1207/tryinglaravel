import { VuexModule, Action, Mutation, Module, getModule } from 'vuex-module-decorators';
import store from '@/store';
import { IMovie } from '../models';
import { getMovies, getMovie, deleteMovie, createMovie } from './api';


@Module({
	namespaced: true,
	name: 'movies',
	store,
	dynamic: true,
})
class MovieModule extends VuexModule {
	private movies: IMovie[] | null = null;
	private movie: IMovie | null = null;

	public get movielist() {
		return this.movies;
	}

	@Action({ commit: 'setMovies' })
	public async getMovies() {
		const movies = await getMovies();
		return movies;
	}

	@Action({ commit: 'setMovie' })
	public async createMovie(movie: IMovie) {
		const movieNew  = await createMovie(movie);
		return movieNew;
	}

	@Action({ commit: 'setMovie' })
	public async getMovie(id: string) {
		const movie = await getMovie(id);
		return movie;
	}

	@Action({ commit: 'deletedMovie' })
	public async deleteMovie(movie: IMovie) {
		deleteMovie(movie.id.toString());
		return movie;
	}

	@Mutation
	private setMovie(movie: IMovie | null) {
		this.movie = movie;
	}

	@Mutation
	private deletedMovie(movie: IMovie){
		if (this.movies !== null) {
			const toDeleteID =this.movies.findIndex( 
				searchMovie => searchMovie.id == movie.id
			);
			this.movies.splice(toDeleteID, 1);
			console.log(this.movies)
		}
	}

	@Mutation
	private setMovies(movies: IMovie[] | null) {
		this.movies = movies;
	}
}

export default getModule(MovieModule);
