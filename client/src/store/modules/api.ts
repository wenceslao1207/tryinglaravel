import axios from 'axios';
import { IMovie, IUserSubmit, IToken } from '../models';
import storage from './localstorage';

const authorization: string = 'Authorization';
export const baseApi = axios.create({
	baseURL: 'http://localhost:8000/api',
});

export function setToken(token: string) {
	baseApi.defaults.headers.common[authorization] = `Bearer ${token}`;
}

export function clearToken() {
	delete baseApi.defaults.headers.common[authorization];
}

export async function loginUser(userSumission: IUserSubmit): Promise<IToken|null> {
	try {
		const response = await baseApi.post('/login', userSumission);
		const token: string = await response.data.success.token;
		setToken(token);
		return (response.data.success.token as IToken);
	} catch (err) {
		console.warn(err);
	}
	return null;
}

export async function getMovies(): Promise<IMovie[]|null> {
	try {
		const response = await baseApi.get('/movies');
		return (response.data.data);
	} catch (err) {
		console.warn(err);
	}
	return null;
}

export async function getMovie(id: string): Promise<IMovie|null> {
	try {
		const response = await baseApi.get(`movies/${id}`);
		return (response.data as IMovie);
	} catch(err) {
		console.error(err);
	}
	return null;
}

export async function createMovie(movie: IMovie): Promise<IMovie|null> {
	try{
		const response = await baseApi.post(`/movies/create`, movie);
		console.log(response);
		return (response.data.data);
	} catch (err) {
		console.error(err);
	}
	return null;
}

export async function deleteMovie(id: string) {
	try {
		const response = await baseApi.delete(`/movies/${id}`);
	} catch (err){
		console.error(err);
	}
}
