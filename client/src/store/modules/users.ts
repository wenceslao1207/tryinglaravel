import { VuexModule, Module, Action, getModule, Mutation } from 'vuex-module-decorators';
import store from '@/store';
import { IUserSubmit, IToken } from '../models';
import { loginUser, clearToken } from './api';
import storage from './localstorage';


@Module({
	namespaced: true,
	name: 'users',
	store,
	dynamic: true,
})
class UserModule extends VuexModule {
	private token: boolean | null = null;

	public get logged() {
		return this.token;
	}

	@Action({ commit: 'setToken' })
	public async login(userRequest: IUserSubmit) {
		const token = await loginUser(userRequest);
		if (token !== null) {
			localStorage.setItem('access_token', token.token);
		}
		return token;
	}

	@Action({ commit: 'setToken'})
	public async logout() {
		localStorage.clear();
		clearToken();
		return null;
	}

	@Mutation
	private setToken(token: IToken | null) {
		if (token !== null) {
			this.token = true;
		} else {
			this.token = false;
		}
	}
}

export default getModule(UserModule);
