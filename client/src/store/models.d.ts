export interface IMovie {
	id: number;
	name: string;
	description: string;
	genere: string;
	actors: [];
}

export interface IActor {
	id: number;
	name: string;
	lastname: string;
	movies?: [];
}

export interface IUserSubmit {
	email: string;
	password: string;
}

export interface IToken {
	token: string;
}
